const anObject = {
    foo: 'bar',
    length: 'interesting',
    '0': 'zero!',
    '1': 'one!'
};

let anArray = ['zero.', 'one.'];
/*
    anArray[0] Accedemos a la primer posición del arreglo
    anObject[0] Los objetos a veces son llamados arreglos asociativos, (Un array asociativo es un array cuyos índices no son numéricos sinó cadenas de texto)
                porque cada propiedad está asociado a un valor de cadena que se pude utilizar para acceder a ella.
*/

console.log(anArray[0], anObject[0])
console.log(anArray[1], anObject[1])

/*
    anArray.length Obtenemos la longitud arreglo
    anObject.length Accedemos a la propiedad length del objeto

    anArray.foo es undefined porque no existe una propiedad llamada foo
    anObject.foo Obtenemos el valor de la propiedad 'foo'
*/

console.log(anArray.length, anObject.length)
console.log(anArray.foo, anObject.foo)

console.log('**********************+')


console.log(typeof anArray === 'object')
console.log(typeof anArray == 'object', typeof anObject == 'object') //true porque typeof identifica un arreglo como un tipo de objeto que está integrado en Js / true porque typeof identifica un objeto como un tipo de objeto que está integrado en Js
console.log(anArray instanceof Object, anObject instanceof Object) // true en ambos porque instanceof verifica un objeto contra la funciòn que se le pase después de la palabra reservada
console.log(anArray instanceof Array, anObject instanceof Array) // true porque se compara un arreglo con la función Array / false porque se está comparando un objeto con una funciòn Array
console.log(Array.isArray(anArray), Array.isArray(anObject)) // isArray verifica si el valor que pasamos por parámetro es un arreglo por lo tanto el primer valor es true y el segundo false