const classInstance = new class {
    get prop() {
        return 5;
    }
};

classInstance.prop = 10; //Aunque asignemos un valor a la porpiedad, siempre se va a retornar el valor 5
console.log(classInstance.prop);