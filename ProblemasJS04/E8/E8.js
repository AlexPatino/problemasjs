class Person {
    #firstName;
    #lastName;
    constructor(FN, LN) {
        this.#firstName = FN; //Usamos el modificador "private" (#) para que los campos no sean accesibles desde el exterior u otras clases
        this.#lastName = LN; //Usamos el modificador "private" (#) para que los campos no sean accesibles desde el exterior u otras clases
    }

    set firstName(value) {
        this.#firstName = value;
    }

    set lastName(value) {
        this.#lastName = value;
    }

    get firstName() {
        return this.#firstName;
    }

    get lastName() {
        return this.#lastName;
    }
}

let person = new Person('Alexis', 'Patiño');

console.log(person.firstName, person.lastName)

person.firstName = 'Foo';
person.lastName = 'Bar';

console.log(person.firstName, person.lastName)