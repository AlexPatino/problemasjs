class Queue {

    constructor() {
        const list = [];

        this.enqueue = function (type) {
            list.push(type);
            return type;
        };

        this.dequeue = function() {
            return list.shift();
        }
    }
}

var q = new Queue;

q.enqueue(9);
q.enqueue(8);
q.enqueue(7);

console.log(q.dequeue()); //shift elimina el primer elemento del arreglo y lo retorna, modificando la longitud del arreglo.
console.log(q.dequeue()); //shift elimina el primer elemento del arreglo y lo retorna, modificando la longitud del arreglo.
console.log(q.dequeue()); //shift elimina el primer elemento del arreglo y lo retorna, modificando la longitud del arreglo.
console.log(q); //Imprime el la instancia de Queue
console.log(Object.keys(q)); //Imprime las propiedades de la clase como un arreglo