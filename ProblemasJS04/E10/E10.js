window.onload = init;

let h = 0;
let m = 0;
let s = 0;

function init() {
    document.getElementById("hms").innerHTML="00 h : 00 min : 00 seg"
}

function cronometrar(){
    escribir();
    id = setInterval(escribir,1000);
}

cronometrar();

function escribir(){
    var hAux, mAux, sAux;
    s++;
    if (s>59){m++;s=0;}
    if (m>59){h++;m=0;}
    if (h>24){h=0;}

    if (s<10){sAux="0"+s;}else{sAux=s;}
    if (m<10){mAux="0"+m;}else{mAux=m;}
    if (h<10){hAux="0"+h;}else{hAux=h;}

    document.getElementById("hms").innerHTML = hAux + " h : " + mAux + " min : " + sAux + " seg";
}