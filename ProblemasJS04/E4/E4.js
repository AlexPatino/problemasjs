let zero = 0;

function multiply(x) {
    return x*2;
}

function add(a = 1 + zero, b = a, c = b + a, d = multiply(c) ) {
    console.log((a + b + c), d)
}

add(1); //a = 1, b = 1, c = 2, d = 4 / 4,4
add(3); //a = 3, b = 3, c = 6, d = 12 / 12,12
add(2, 7); //a = 2, b = 7, c = 9, d = 18 / 18,18
add(1, 2, 5); //a = 1, b = 2, c = 5, d = 18 / 8,10
add(1, 2, 5, 10); //a = 1, b = 2, c = 5, d = 10 / 8,10