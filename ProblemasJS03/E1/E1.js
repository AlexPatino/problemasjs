let coords = document.getElementById('coords');
let key = document.getElementById('key');

function coordenadas(event) {
    coords.innerHTML = 'Pagina: [' + event.clientX + ', ' + event.clientY + ']';
}

function teclado(event) {
    key.innerHTML = 'Carácter: ' + `[${event.key}]`
}

window.addEventListener('mousemove', coordenadas);
window.addEventListener('keypress', teclado);