
let usa = document.getElementById('C1');
let canada = document.getElementById('C2');
let francia = document.getElementById('C3');

usa.addEventListener('click', (e) => {
    alert(`
        Elemento Seleccionado:
        "ID elemento: ${usa.id}"
        "ISO ID: ${usa.getAttribute('data-id')}"
        "Dial Code: ${usa.getAttribute('data-dial-code')}"
    `)
})

canada.addEventListener('click', (e) => {
    alert(`
        Elemento Seleccionado:
        "ID elemento: ${canada.id}"
        "ISO ID: ${canada.getAttribute('data-id')}"
        "Dial Code: ${canada.getAttribute('data-dial-code')}"
    `)
})

francia.addEventListener('click', (e) => {
    alert(`
        Elemento Seleccionado:
        "ID elemento: ${francia.id}"
        "ISO ID: ${francia.getAttribute('data-id')}"
        "Dial Code: ${francia.getAttribute('data-dial-code')}"
    `)
})