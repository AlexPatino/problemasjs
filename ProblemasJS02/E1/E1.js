var personArr = [
    {
        "personId": 123,
        "name": "Jhon",
        "city": "Melbourne",
        "phoneNo": "1234567890"
    },
    {
        "personId": 124,
        "name": "Amelia",
        "city": "Sydney",
        "phoneNo": "1234567890"
    },
    {
        "personId": 125,
        "name": "Emily",
        "city": "Perth",
        "phoneNo": "1234567890"
    },
    {
        "personId": 126,
        "name": "Abraham",
        "city": "Perth",
        "phoneNo": "1234567890"
    }
];

let titulos = ["PersonId", "Nombre", "Ciudad", "Número"]

const crearTabla = () => {
    let body = document.getElementsByTagName("body")[0];
    let table = document.createElement('table');
    let tbody = document.createElement('tbody');
    
    for (let i = 0; i < 1; i++) {
        let row = document.createElement('tr');
        Object.values(titulos).forEach(j => {
            let fila = document.createElement('td');
            let txtFila = document.createTextNode(j);
            fila.appendChild(txtFila);
            row.appendChild(fila);
        })
        tbody.appendChild(row);
    }

    personArr.forEach(x => {
        let row = document.createElement('tr');
        Object.values(x).forEach(y => {
            let fila = document.createElement('td');
            let txtFila = document.createTextNode(y);
            fila.appendChild(txtFila);
            row.appendChild(fila);
        })
        tbody.appendChild(row);
    })
    table.append(tbody);
    body.appendChild(table);
    table.setAttribute("border", "2");
}

crearTabla()